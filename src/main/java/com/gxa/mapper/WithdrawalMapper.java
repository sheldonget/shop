package com.gxa.mapper;

import com.gxa.pojo.Withdrawal;

public interface WithdrawalMapper {
    int deleteByPrimaryKey(Integer withdrawalId);

    int insert(Withdrawal record);

    int insertSelective(Withdrawal record);

    Withdrawal selectByPrimaryKey(Integer withdrawalId);

    int updateByPrimaryKeySelective(Withdrawal record);

    int updateByPrimaryKey(Withdrawal record);
}