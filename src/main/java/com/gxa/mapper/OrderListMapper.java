package com.gxa.mapper;

import com.gxa.pojo.OrderList;

public interface OrderListMapper {
    int deleteByPrimaryKey(Integer orderId);

    int insert(OrderList record);

    int insertSelective(OrderList record);

    OrderList selectByPrimaryKey(Integer orderId);

    int updateByPrimaryKeySelective(OrderList record);

    int updateByPrimaryKey(OrderList record);
}