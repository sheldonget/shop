package com.gxa.mapper;

import com.gxa.pojo.RoleAuth;

public interface RoleAuthMapper {
    int deleteByPrimaryKey(Integer raId);

    int insert(RoleAuth record);

    int insertSelective(RoleAuth record);

    RoleAuth selectByPrimaryKey(Integer raId);

    int updateByPrimaryKeySelective(RoleAuth record);

    int updateByPrimaryKey(RoleAuth record);
}