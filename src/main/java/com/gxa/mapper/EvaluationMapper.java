package com.gxa.mapper;

import com.gxa.pojo.Evaluation;

public interface EvaluationMapper {
    int deleteByPrimaryKey(Byte evaluationId);

    int insert(Evaluation record);

    int insertSelective(Evaluation record);

    Evaluation selectByPrimaryKey(Byte evaluationId);

    int updateByPrimaryKeySelective(Evaluation record);

    int updateByPrimaryKey(Evaluation record);
}