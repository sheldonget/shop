package com.gxa.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableWebMvc
@EnableSwagger2
public class MySwaggerConfig {
    @Bean
    public Docket docket(){
        Docket docket = new Docket(DocumentationType.SWAGGER_2);
        ApiInfo apiInfo = new ApiInfoBuilder()
                .title("Api[接口文档]")
                .description("描述")
                .contact(new Contact("lsj", "url", "邮箱"))
                .version("v1.0")
                .build();
        // 只会扫描到有对应注解的方法
 /*docket.select().apis(RequestHandlerSelectors.withClassAnnotation(Api.class))
 .apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))
 .build();*/
        docket
                .apiInfo(apiInfo)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.gxa.controller"))
                .paths(PathSelectors.any())
                .build();
        return docket;
    } }