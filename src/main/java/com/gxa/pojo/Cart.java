package com.gxa.pojo;

import java.io.Serializable;
import lombok.Data;

/**
 * cart
 * @author 
 */
@Data
public class Cart implements Serializable {
    private Integer cartId;

    /**
     * 用户id
     */
    private Integer userId;

    /**
     * 商品id
     */
    private Integer goodsId;

    /**
     * 添加至购物车的商品数量
     */
    private Integer cartGoodsNumber;

    private static final long serialVersionUID = 1L;
}