package com.gxa.pojo;

import java.io.Serializable;
import lombok.Data;

/**
 * group
 * @author 
 */
@Data
public class Group implements Serializable {
    private Integer groupId;

    /**
     * 分类名称
     */
    private String groupName;

    private static final long serialVersionUID = 1L;
}