package com.gxa.pojo;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * order_list
 * @author 
 */
@Data
public class OrderList implements Serializable {
    private Integer orderId;

    /**
     * 订单编号
     */
    private String orderNumber;

    /**
     * 商品id
     */
    private Integer goodsId;

    /**
     * 商品数量
     */
    private Integer goodsNumber;

    /**
     * 收货人id
     */
    private Integer userId;

    /**
     * 订单总金额
     */
    private Double orderTotalMoney;

    /**
     * 订单状态   0是未支付  1是已支付
     */
    private Integer orderStatus;

    /**
     * 订单发货状态  0是未发货  1是待发货  2是已发货  3是已收货
     */
    private Integer orderSendStatus;

    /**
     * 订单是否取消  0是未取消  1是已取消
     */
    private Integer orderIsCancel;

    /**
     * 下单时间
     */
    private Date orderTime;

    /**
     * 付款时间
     */
    private Date orderPayTime;

    /**
     * 快递公司名称
     */
    private String orderCourier;

    /**
     * 快递单号
     */
    private String orderCourierId;

    /**
     * 订单完成时间
     */
    private Date orderFinishTime;

    /**
     * 订单留言
     */
    private String orderMessage;

    private static final long serialVersionUID = 1L;
}