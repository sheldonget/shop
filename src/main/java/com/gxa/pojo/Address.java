package com.gxa.pojo;

import java.io.Serializable;
import lombok.Data;

/**
 * address
 * @author 
 */
@Data
public class Address implements Serializable {
    private Integer addressId;

    /**
     * 收货地址
     */
    private String addressName;

    /**
     * 收件人名字
     */
    private String addressReceiverName;

    /**
     * 收货电话
     */
    private Long addressReceiverPhone;

    /**
     * 用户id
     */
    private Integer userId;

    /**
     * 地址是否删除  0是未删除  1是已删除
     */
    private Integer addressIsDelete;

    private static final long serialVersionUID = 1L;
}