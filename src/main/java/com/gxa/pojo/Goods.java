package com.gxa.pojo;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * goods
 * @author 
 */
@Data
public class Goods implements Serializable {
    private Integer goodsId;

    /**
     * 商品名称
     */
    private String goodsName;

    /**
     * 商品分类id
     */
    private Integer goodsGroupId;

    /**
     * 商品图片
     */
    private String goodsPic;

    /**
     * 商品市场价
     */
    private Double goodsHighPrice;

    /**
     * 商品售价
     */
    private Double goodsRealPrice;

    /**
     * 库存
     */
    private Integer goodsNumber;

    /**
     * 商品描述框
     */
    private String goodsDescText;

    /**
     * 商品销量
     */
    private Integer goodsSellNumber;

    /**
     * 商品上架时间
     */
    private Date goodsLaunchTime;

    /**
     * 商品尺寸
     */
    private String goodsSize;

    /**
     * 商品颜色
     */
    private String goodsColor;

    /**
     * 商品品牌
     */
    private String goodsBrand;

    /**
     * 商品产地
     */
    private String goodsPlace;

    /**
     * 商品是否下架  0是未下架  1是已下架
     */
    private Integer goodsIsDelete;

    /**
     * 店铺id
     */
    private Integer shopId;

    private static final long serialVersionUID = 1L;
}