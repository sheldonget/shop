package com.gxa.pojo;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * admin
 * @author 
 */
@Data
public class Admin implements Serializable {
    private Integer adminId;

    /**
     * 管理员账号
     */
    private String adminName;

    /**
     * 管理员密码
     */
    private String adminPwd;

    /**
     * 权限    0是超级管理员  1商家  2是普通管理员
     */
    private Integer adminRole;

    /**
     * 店铺的id
     */
    private Integer shopId;

    /**
     * 创建时间
     */
    private Date adminCreateTime;

    /**
     * 最后登录时间
     */
    private Date adminLastLoginTime;

    /**
     * 管理员是否被删除 0是未删除  1是已删除
     */
    private Integer adminIsDelete;

    private static final long serialVersionUID = 1L;
}