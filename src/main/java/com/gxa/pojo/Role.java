package com.gxa.pojo;

import java.io.Serializable;
import lombok.Data;

/**
 * role
 * @author 
 */
@Data
public class Role implements Serializable {
    private Integer roleId;

    /**
     * 角色名字
     */
    private String roleName;

    /**
     * 角色描述
     */
    private String roleDesc;

    /**
     * 0 不是 1 是
     */
    private Integer isSuper;

    private static final long serialVersionUID = 1L;
}