package com.gxa.pojo;

import java.io.Serializable;
import lombok.Data;

/**
 * role_auth
 * @author 
 */
@Data
public class RoleAuth implements Serializable {
    private Integer raId;

    /**
     * 角色id
     */
    private Integer roleId;

    /**
     * 权限id
     */
    private Integer authId;

    private static final long serialVersionUID = 1L;
}