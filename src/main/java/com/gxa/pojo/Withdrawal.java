package com.gxa.pojo;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * withdrawal
 * @author 
 */
@Data
public class Withdrawal implements Serializable {
    private Integer withdrawalId;

    /**
     * 店铺id
     */
    private Integer shopId;

    /**
     * 提现金额
     */
    private Double withdrawalMoney;

    /**
     * 提现审核状态  0未审核  1已审核
     */
    private Integer withdrawalStatus;

    /**
     * 是否同意提现 0是不同意  1是同意
     */
    private Integer withdrawalIsok;

    /**
     * 提现申请时间
     */
    private Date withdrawalApplyTime;

    /**
     * 提现同意时间
     */
    private Date withdrawalOkTime;

    private static final long serialVersionUID = 1L;
}