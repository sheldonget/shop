package com.gxa.pojo;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * user
 * @author 
 */
@Data
public class User implements Serializable {
    private Integer userId;

    /**
     * 用户名
     */
    private String userName;

    /**
     * 密码
     */
    private String userPwd;

    /**
     * 电话
     */
    private Long userPhone;

    /**
     * 性别  0是男  1是女
     */
    private Integer userSex;

    /**
     * 邮箱
     */
    private String userEmail;

    /**
     * 用户状态  0是正常  1是被拉黑
     */
    private Integer userStatus;

    /**
     * 用户注册时间
     */
    private Date userCreateTime;

    /**
     * 用户最后登录时间
     */
    private Date userLastLoginTime;

    private static final long serialVersionUID = 1L;
}