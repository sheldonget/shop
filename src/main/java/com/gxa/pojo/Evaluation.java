package com.gxa.pojo;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * evaluation
 * @author 
 */
@Data
public class Evaluation implements Serializable {
    private Integer evaluationId;

    /**
     * 订单id
     */
    private Integer orderId;

    /**
     * 评价话
     */
    private String evaluationDesc;

    /**
     * 评价等级
     */
    private Integer evaluationLevel;

    /**
     * 用户名
     */
    private String userName;

    /**
     * 评论时间
     */
    private Date evaluationTime;

    private static final long serialVersionUID = 1L;
}