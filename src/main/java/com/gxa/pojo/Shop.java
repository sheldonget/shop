package com.gxa.pojo;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * shop
 * @author 
 */
@Data
public class Shop implements Serializable {
    private Integer shopId;

    /**
     * 商铺名
     */
    private String shopName;

    /**
     * 店铺创建时间
     */
    private Date shopCreateTime;

    /**
     * 店铺成交件数
     */
    private Integer shopTotalNumber;

    /**
     * 店铺营业额
     */
    private Double shopTurnover;

    /**
     * 店铺状态  1 已审核  0 未审核
     */
    private Integer shopStatus;

    /**
     * 店铺标志
     */
    private String shopLogo;

    /**
     * 店铺申请人姓名
     */
    private String shopProposerName;

    /**
     * 店铺申请人头像
     */
    private String shopProposerPic;

    /**
     * 店铺账户
     */
    private String userName;

    /**
     * 店铺描述
     */
    private String shopDesc;

    /**
     * 店铺分类
     */
    private String shopGroup;

    /**
     * 店铺操作  0是同意  1是驳回
     */
    private Integer shopOperation;

    /**
     * 店铺操作驳回理由
     */
    private String shopOperationReason;

    /**
     * 店铺地址
     */
    private String shopAddress;

    /**
     * 店铺营业执照
     */
    private String shopLicense;

    /**
     * 店铺营业执照允许时间
     */
    private Date shopLicenseTime;

    /**
     * 商家联系电话
     */
    private Long shopPhone;

    /**
     * 店铺描述图片
     */
    private String shopDescPic;

    /**
     * 店铺余额
     */
    private Double shopMoney;

    private static final long serialVersionUID = 1L;
}