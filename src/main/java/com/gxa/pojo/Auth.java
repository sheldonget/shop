package com.gxa.pojo;

import java.io.Serializable;
import lombok.Data;

/**
 * auth
 * @author 
 */
@Data
public class Auth implements Serializable {
    private Integer authId;

    /**
     * 权限名字
     */
    private String authName;

    /**
     * 权限url路径
     */
    private String authUrl;

    /**
     * 是否为菜单:1-是,2-不是
     */
    private Integer isMenu;

    /**
     * 父级ID
     */
    private Integer authParentId;

    private static final long serialVersionUID = 1L;
}