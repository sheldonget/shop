package com.gxa.pojo;

import java.io.Serializable;
import lombok.Data;

/**
 * admin_role
 * @author 
 */
@Data
public class AdminRole implements Serializable {
    private Integer arId;

    /**
     * 管理员id
     */
    private Integer adminId;

    /**
     * 角色id
     */
    private Integer roleId;

    private static final long serialVersionUID = 1L;
}